# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 07:36+0200\n"
"PO-Revision-Date: 2015-02-24 23:44+0100\n"
"Last-Translator: Samir Ribić <megaribi@epn.ba>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 17341)\n"
"X-Launchpad-Export-Date: 2015-02-15 06:27+0000\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Timur Ćerimagić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tcerimagic1@etf.unsa.ba"

#: kdeconnect-cli.cpp:43
#, kde-format
msgid "KDE Connect CLI tool"
msgstr "KDE Veza CLI alat"

#: kdeconnect-cli.cpp:45
#, fuzzy, kde-format
#| msgid "(C) 2013 Aleix Pol Gonzalez"
msgid "(C) 2015 Aleix Pol Gonzalez"
msgstr "(C) 2013 Aleix Pol Gonzalez"

#: kdeconnect-cli.cpp:48
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: kdeconnect-cli.cpp:49
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kdeconnect-cli.cpp:51
#, fuzzy, kde-format
#| msgid "List all devices."
msgid "List all devices"
msgstr "Lista svih uređaja."

#: kdeconnect-cli.cpp:52
#, kde-format
msgid "List available (paired and reachable) devices"
msgstr ""

#: kdeconnect-cli.cpp:53
#, kde-format
msgid ""
"Make --list-devices or --list-available print only the devices id, to ease "
"scripting"
msgstr ""

#: kdeconnect-cli.cpp:54
#, kde-format
msgid ""
"Make --list-devices or --list-available print only the devices name, to ease "
"scripting"
msgstr ""

#: kdeconnect-cli.cpp:55
#, kde-format
msgid ""
"Make --list-devices or --list-available print only the devices id and name, "
"to ease scripting"
msgstr ""

#: kdeconnect-cli.cpp:56
#, fuzzy, kde-format
#| msgid "Search for devices in the network and re-establish connections."
msgid "Search for devices in the network and re-establish connections"
msgstr "Potraži uređaje na mreži i ponovo uspostavi konekciju."

#: kdeconnect-cli.cpp:57
#, fuzzy, kde-format
#| msgid "Request pairing to a said device."
msgid "Request pairing to a said device"
msgstr "Zatraži uparivanje sa navedenim uređajem."

#: kdeconnect-cli.cpp:58
#, kde-format
msgid "Find the said device by ringing it."
msgstr ""

#: kdeconnect-cli.cpp:59
#, fuzzy, kde-format
#| msgid "Stop pairing to a said device."
msgid "Stop pairing to a said device"
msgstr "Zaustavi uparivanje sa navedenim uređajem."

#: kdeconnect-cli.cpp:60
#, fuzzy, kde-format
#| msgid "Send a ping to said device."
msgid "Sends a ping to said device"
msgstr "Pošalji ping navedenom uređaju."

#: kdeconnect-cli.cpp:61
#, fuzzy, kde-format
#| msgid "Same as ping but you can customize the message shown."
msgid "Same as ping but you can set the message to display"
msgstr "Isto kao i ping ali možete podesiti poruku koja se prikazuje."

#: kdeconnect-cli.cpp:61 kdeconnect-cli.cpp:65
#, kde-format
msgid "message"
msgstr ""

#: kdeconnect-cli.cpp:62
#, fuzzy, kde-format
#| msgid "Share a file to a said device."
msgid "Share a file to a said device"
msgstr "Podijeli datoteku na navedeni uređaj."

#: kdeconnect-cli.cpp:63
#, fuzzy, kde-format
#| msgid "Display the notifications on a said device."
msgid "Display the notifications on a said device"
msgstr "Prikaži notifikaciju na navedenom uređaju."

#: kdeconnect-cli.cpp:64
#, kde-format
msgid "Lock the specified device"
msgstr ""

#: kdeconnect-cli.cpp:65
#, kde-format
msgid "Sends an SMS. Requires destination"
msgstr ""

#: kdeconnect-cli.cpp:66
#, kde-format
msgid "Phone number to send the message"
msgstr ""

#: kdeconnect-cli.cpp:66
#, kde-format
msgid "phone number"
msgstr ""

#: kdeconnect-cli.cpp:67
#, fuzzy, kde-format
#| msgid "Device ID."
msgid "Device ID"
msgstr "Identifikacijski Broj Uređaja."

#: kdeconnect-cli.cpp:68
#, fuzzy, kde-format
#| msgid "Device ID."
msgid "Device Name"
msgstr "Identifikacijski Broj Uređaja."

#: kdeconnect-cli.cpp:69
#, fuzzy, kde-format
#| msgid "Request pairing to a said device."
msgid "Get encryption info about said device"
msgstr "Zatraži uparivanje sa navedenim uređajem."

#: kdeconnect-cli.cpp:70
#, kde-format
msgid "Lists remote commands and their ids"
msgstr ""

#: kdeconnect-cli.cpp:71
#, kde-format
msgid "Executes a remote command by id"
msgstr ""

#: kdeconnect-cli.cpp:72
#, fuzzy, kde-format
#| msgid "Send a ping to said device."
msgid "Sends keys to a said device"
msgstr "Pošalji ping navedenom uređaju."

#: kdeconnect-cli.cpp:73
#, kde-format
msgid "Display this device's id and exit"
msgstr ""

#: kdeconnect-cli.cpp:114
#, kde-format
msgid "(paired and reachable)"
msgstr ""

#: kdeconnect-cli.cpp:116
#, kde-format
msgid "(reachable)"
msgstr ""

#: kdeconnect-cli.cpp:118
#, kde-format
msgid "(paired)"
msgstr ""

#: kdeconnect-cli.cpp:125
#, fuzzy, kde-format
#| msgid "No device specified"
msgid "1 device found"
msgid_plural "%1 devices found"
msgstr[0] "Nije određen uređaj"
msgstr[1] "Nije određen uređaj"

#: kdeconnect-cli.cpp:127
#, fuzzy, kde-format
#| msgid "No device specified"
msgid "No devices found"
msgstr "Nije određen uređaj"

#: kdeconnect-cli.cpp:145
#, kde-format
msgid "No device specified"
msgstr "Nije određen uređaj"

#: kdeconnect-cli.cpp:158
#, kde-format
msgid "Can't find the file: %1"
msgstr ""

#: kdeconnect-cli.cpp:169
#, kde-format
msgid "Sent %1"
msgstr ""

#: kdeconnect-cli.cpp:176
#, kde-format
msgid "waiting for device..."
msgstr ""

#: kdeconnect-cli.cpp:190
#, fuzzy, kde-format
#| msgid "No device specified"
msgid "Device not found"
msgstr "Nije određen uređaj"

#: kdeconnect-cli.cpp:192
#, kde-format
msgid "Already paired"
msgstr ""

#: kdeconnect-cli.cpp:194
#, kde-format
msgid "Pair requested"
msgstr ""

#: kdeconnect-cli.cpp:201
#, kde-format
msgid "Device does not exist"
msgstr ""

#: kdeconnect-cli.cpp:203
#, kde-format
msgid "Already not paired"
msgstr ""

#: kdeconnect-cli.cpp:205
#, kde-format
msgid "Unpaired"
msgstr ""

#: kdeconnect-cli.cpp:221
#, kde-format
msgid ""
"error: should specify the SMS's recipient by passing --destination <phone "
"number>"
msgstr ""

#: kdeconnect-cli.cpp:268
#, fuzzy, kde-format
#| msgid "Nothing to be done with the device"
msgid "Nothing to be done"
msgstr "Nema ništa da se uradi sa uređajem"